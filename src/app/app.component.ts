import { Component } from '@angular/core';

//tambien se puede usar template con ` ` y dentro escribir el codigo html para asi tener todo junto
//pero con el templateUrl le haces saber al programa donde esta tu codigo html y lo trabajas por separado.


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})

export class AppComponent {
  
  }

