import { Component} from '@angular/core';
import { Personaje } from '../interfaces/dbz.interface';

import { DbzService } from '../services/dbz.service';



@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html'
})

export class MainPageComponent {

  nuevo: Personaje = {
    nombre: '',
    poder: 0
  }

  get personajes(): Personaje[] {
    return this.dbzService.personajes;
  }

/*   agregarNuevoPersonaje( event: Personaje) {
    this.personajes.push(event);
  }  */ //este metodo esataba vinculado con el output de nuevo personaje

  
  constructor(private dbzService: DbzService){
    //esto se conoce como una inyeccion de dependencias
  }
}

