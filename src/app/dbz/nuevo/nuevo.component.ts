import { Component, Input} from '@angular/core';
import { Personaje } from '../interfaces/dbz.interface';
import { DbzService } from '../services/dbz.service';

@Component({
  selector: 'app-nuevo',
  templateUrl: './nuevo.component.html'
})

export class NuevoComponent  {

  @Input() nuevo: Personaje = {
    nombre: '',
    poder: 0
  };
  
  constructor(private dbzService: DbzService){}

  //emite el personajea a traves de un evento
  //@Output() onNuevoPersonaje: EventEmitter<Personaje> = new EventEmitter<Personaje>();

  agregar() {

    if (this.nuevo.nombre.trim().length === 0){
        return;
    }

    //this.onNuevoPersonaje.emit(this.nuevo); 
    this.dbzService.agregarPersonaje(this.nuevo);
    
    this.nuevo = {
      nombre: '',
      poder: 0
    }
  }
}
