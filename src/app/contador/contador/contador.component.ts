import { Component } from "@angular/core";

@Component({
    selector: 'app-contador',
    template: `
            <h1>{{titulo}}</h1> 
        <label>Ingrese una base nueva si lo desea: </label>
        <input #nuevaBase><button (click)=" actualizarBase(nuevaBase.value) ">Cargar</button>
        <h3>La base actual es: <strong>{{base}}</strong></h3>


        <button (click)=" acumular(-base) ">-</button>
        <span>{{numero}}</span>
        <button (click)=" acumular(base) ">+</button>
    `
})

export class ContadorComponent{
    
    titulo: string = 'Acumulador App';
    numero: number = 10;
    base: number = 5;
    
    acumular(valor: number) {
        this.numero += valor;
    }
   
    actualizarBase (valor: string) {
      this.base = parseInt(valor);
}}